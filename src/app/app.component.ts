import { Component, OnInit, } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { FormBuilder, FormGroup, Validators, } from '@angular/forms';

import { CustomValidators } from 'ng2-validation';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent implements OnInit {

  isLinear: boolean = true;

  public locationFormGroup: FormGroup;
  public workingHoursFormGroup: FormGroup;
  public contactFormGroup: FormGroup;
  public photosFormGroup: FormGroup;
  public filtresFormGroup: FormGroup;
  public additionsAndPriceFromGroup: FormGroup;

  constructor(
    private translate: TranslateService,
    private _formBuilder: FormBuilder) {}

  switchLanguage(language: string) {
    this.translate.use(language);
    localStorage.setItem('lang', language);
  }

  ngOnInit() {

    const language = localStorage.getItem('lang');
    this.translate.setDefaultLang(language ? language : 'en');

    this.locationFormGroup = this._formBuilder.group({
      juridicName: [{
        value: '',
        disabled: true
      }, Validators.maxLength(50)],
      commercialName: ['', [Validators.required, Validators.maxLength(50)]],
      StarnetSale: ['', [Validators.required, CustomValidators.range([7, 100])]],
      bronari: [false, Validators.required],
      categories: ['', Validators.required],
      averageSum: ['', Validators.required],
      desciptionRO: ['', [Validators.required, Validators.maxLength(100)]],
      desciptionRU: ['', [Validators.required, Validators.maxLength(100)]]
    });

    this.workingHoursFormGroup = this._formBuilder.group({
      from: [null],
      to: [null],
      program: [null, Validators.required]
    }, {
      validator: this.validateProgram.bind(this)
    });

    this.contactFormGroup = this._formBuilder.group({
      phone: [null, [Validators.required, Validators.minLength(8)]],
      longitude: [null, Validators.required],
      latitude: [null, Validators.required],
      city: [null, Validators.required],
      country: [null, Validators.required],
      street: [null, Validators.required]
    });

    this.photosFormGroup = this._formBuilder.group({
      imageURL: [null, Validators.required]
    });

    this.filtresFormGroup = this._formBuilder.group({
      filters: [[], Validators.required]
    });

    this.additionsAndPriceFromGroup = this._formBuilder.group({
      items: [[], Validators.required],
      additions: [[], Validators.required]
    });
  }

  validateProgram(formGroup) {
    if (formGroup.controls['program'].value === 'open') {
      if (!formGroup.controls['from'].value || !formGroup.controls['to'].value) {
        return {
          missingProgram: true
        };
      } else if (!this.hourMask(formGroup.controls['from'].value) ||
        !this.hourMask(formGroup.controls['to'].value)) {
        return {
          invalidProgram: true
        };
      }
    }
    return null;
  }

  hourMask(text) {
    return /^([01][0-9]|2[0-3])[0-5][0-9]$/.exec(text);
  }

  onSubmit() {
    const body = {
      'location': this.locationFormGroup.value,
      'contact': this.contactFormGroup.value,
      'photo': this.photosFormGroup.value,
      'filtres': this.filtresFormGroup.value,
      'additions': this.additionsAndPriceFromGroup.value,
      'hours': this.workingHoursFormGroup.value
    };

    console.log(body);
  }

}

