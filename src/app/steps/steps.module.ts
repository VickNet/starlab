import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaskModule } from 'soft-angular-mask';
import { Ng2ImgToolsModule } from 'ng2-img-tools';
import { TranslateModule } from '@ngx-translate/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown';

import { AgmCoreModule } from '@agm/core';
const GoogleMapsModule = AgmCoreModule.forRoot({
  apiKey: 'AIzaSyBNufqn5gW453A5JNnIwBp7Z-6QxGsaG-I',
  language: 'ro',
  libraries: ['geometry', 'places']
});

import { PhotosComponent } from './photos';
import { FiltersComponent } from './filters';
import { ContactComponent } from './contact';
import { LocationComponent } from './location';
import { WorkingHoursComponent } from './working-hours';
import { AdditionsAndPriceComponent } from './additions-and-price';

import { MaterialModule } from '../material';
import { StepsService } from './steps.service';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    MaterialModule,
    ReactiveFormsModule,
    TranslateModule,
    GoogleMapsModule,
    MaskModule,
    Ng2ImgToolsModule,
    HttpClientModule,
    AngularMultiSelectModule
  ],
  declarations: [
    PhotosComponent,
    FiltersComponent,
    ContactComponent,
    LocationComponent,
    WorkingHoursComponent,
    AdditionsAndPriceComponent
  ],
  exports: [
    PhotosComponent,
    FiltersComponent,
    ContactComponent,
    LocationComponent,
    WorkingHoursComponent,
    AdditionsAndPriceComponent
  ],
  providers: [
    StepsService
  ]
})
export class StepsModule { }
