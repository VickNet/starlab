import { Component, OnInit, Input } from '@angular/core';
import { Ng2ImgToolsService } from 'ng2-img-tools';
import { FormGroup } from '@angular/forms'
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'photos',
  templateUrl: './photos.component.html',
  styleUrls: ['./photos.component.scss']
})
export class PhotosComponent implements OnInit {

  @Input() photosFormGroup: FormGroup;
  constructor(
    private ng2ImgToolsService: Ng2ImgToolsService,
    private http: HttpClient) {}

  public loading: boolean = false;
  public image: string;
  public imageURL: string;

  public prework(evt) {
    const files = evt.target.files;
    const file = files[0];

    if (files && file) {
      this.resize(file);
    }
  }

  ngOnInit() {
    this.imageURL = localStorage.getItem('imageURL');
  }

  public saveForm() {
    localStorage.setItem('imageURL', this.imageURL);
  }

  public resetForm() {
    localStorage.removeItem('imageURL');
    this.photosFormGroup.reset();
  }

  private resize(image) {
    this.loading = true;
    this.imageURL = '';
    this.ng2ImgToolsService.resizeExactCrop([image], 750, 452).subscribe(
      resized => {
        if (resized['size'] > 1000000) {
          this.ng2ImgToolsService.compress([resized], 1).subscribe(
            compressed => {
              this.uploadToImgur(compressed);
            }, e => {
              console.log(e);
            });
        } else {
          this.uploadToImgur(resized);
        }
      }, err => {
        console.log(err);
      });
  }

  private uploadToImgur(blob) {
    const reader = new FileReader();
    reader.readAsBinaryString(blob); /* convert blob to raw binary string */
    reader.onloadend = () => {
      const image = reader.result; /* get raw file data from blob */
      this.http
        .post('https://api.imgur.com/3/image', {
          image: btoa(image) /* convert rae file data to base64 */
        }, {
          headers: new HttpHeaders({
            'Authorization': 'Client-ID cd20c27def752e6'
          })
        })
        .subscribe(
          data => {
            this.imageURL = data['data'].link;
            this.loading = false;
            this.saveForm();
          }, err => {
            console.log(err);
          });
    };
  }

}
