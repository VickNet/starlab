import { Component, OnInit, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

import { StepsService } from '../steps.service';

@Component({
  selector: 'additions-and-price',
  templateUrl: './additions-and-price.component.html',
  styleUrls: ['./additions-and-price.component.scss']
})
export class AdditionsAndPriceComponent implements OnInit {

  @Input() additionsAndPriceFromGroup: FormGroup;
  settings = {};
  
  mainList = [];
  mainItems = [];
  additionalList = [];
  additionalItems = [];
  
  constructor(private _service: StepsService) {}

  ngOnInit() {

    this._service.getAdditionsData().subscribe(
      data => {
        this.mainList = data['prices'];
      });

    this.settings = {
      enableCheckAll: false
    };

    const data = JSON.parse(localStorage.getItem('additions'));
    if (data) {
      this.mainItems = data.items;
      this.additionalItems = data.additions;
    }

  }

  onItemSelect(item: any) {
    this.additionalList = [...item.additions, ...this.additionalList];
  }

  OnItemDeSelect(item: any) {
    this.additionalItems = [];
    this.additionalList = this.additionalList.filter(function (el) {
      return !item.additions.includes(el);
    });
  }

  resetForm() {
    localStorage.removeItem('additions');
    this.additionsAndPriceFromGroup.reset();
  }

  saveForm() {
    localStorage.setItem('additions', JSON.stringify(this.additionsAndPriceFromGroup.value))
  }

}

