import { ElementRef, Component, OnInit, NgZone, Input, ViewChild } from '@angular/core';
import { FormGroup } from '@angular/forms';
import {} from '@types/googlemaps';
import { MapsAPILoader } from '@agm/core';

@Component({
  selector: 'contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {

  @Input() contactFormGroup: FormGroup;

  @ViewChild('address') public address: ElementRef;

  public longitude: number;
  public latitude: number;
  public phone: string;

  public country: string;
  public city: string;
  public street: string;

  private getGeoLocation(lat: number, lng: number) {
    this.latitude = lat;
    this.longitude = lng;
    if (navigator.geolocation) {
      const geocoder = new google.maps.Geocoder();
      const latLng = new google.maps.LatLng(lat, lng);

      geocoder.geocode({
        location: latLng
      }, (results, status) => {
        if (status === google.maps.GeocoderStatus.OK) {
          const result = results[0];
          if (result != null) {
            const array = result.formatted_address.split(',');
            const n = array.length - 2;
            this.country = array[n + 1];
            this.city = array[n];
            this.street = array[n - 1];
          } else {
            alert(`No address available!`);
          }
        }
      });
    }
  }

  constructor(
    private mapsAPILoader: MapsAPILoader,
    private ngZone: NgZone
  ) {}

  ngOnInit() {

    this.latitude = 47.0105;
    this.longitude = 28.8638;

    const data = localStorage.getItem('contact');
    if (data) {
      const s = JSON.parse(data);
      this.phone = s.phone;
      this.latitude = s.latitude;
      this.longitude = s.longitude;
      this.country = s.country;
      this.city = s.city;
      this.street = s.street;
    }

    this.mapsAPILoader.load().then(() => {

      const autocomplete = new google.maps.places.Autocomplete(this.address.nativeElement, {
        types: ['address']
      });

      autocomplete.addListener('place_changed', () => {
        this.ngZone.run(() => {
          const place: google.maps.places.PlaceResult = autocomplete.getPlace();

          if (place.geometry === undefined || place.geometry === null) {
            return;
          }

          this.getGeoLocation(place.geometry.location.lat(), place.geometry.location.lng());
        });
      });
    });
  }

  public saveForm() {
    localStorage.setItem('contact', JSON.stringify(this.contactFormGroup.getRawValue()));
  }

  public resetForm() {
    this.contactFormGroup.reset();
    localStorage.removeItem('contact');
  }

  public mapping(marker) {
    this.getGeoLocation(marker.coords.lat, marker.coords.lng);
  }

}

