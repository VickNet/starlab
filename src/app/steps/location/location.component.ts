import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { StepsService } from '../steps.service';

@Component({
  selector: 'location',
  templateUrl: './location.component.html',
  styleUrls: ['./location.component.scss']
})
export class LocationComponent implements OnInit {

  public juridicName: string;
  public prices: string[];
  public categories: number[];

  @Input() locationFormGroup: FormGroup;

  constructor(private _service: StepsService) {}

  ngOnInit() {
    const data = localStorage.getItem('location');
    if (data) {
      this.locationFormGroup.patchValue(JSON.parse(data));
    }
    this._service.getLocationData().subscribe(
      data => {
        this.juridicName = data['juridicName'];
        this.prices = data['prices'];
        this.categories = data['categories'];
      });
  }

  public saveForm() {
    localStorage.setItem('location', JSON.stringify(this.locationFormGroup.value));
  }

  public resetForm() {
    this.locationFormGroup.reset({
      juridicName: this.juridicName
    });
    localStorage.removeItem('location');
  }

}

